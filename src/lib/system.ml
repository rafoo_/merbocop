open! Base

let command_to_string_list cmd =
  let i = Unix.open_process_in cmd in
  let rec loop acc =
    try loop (Caml.input_line i :: acc)
    with _ -> Caml.close_in i ; List.rev acc in
  let lines = loop [] in
  let status = Unix.close_process_in i in
  (lines, status)

let command_to_string_list_or_fail cmd =
  match command_to_string_list cmd with
  | l, Unix.WEXITED 0 -> l
  | _, _ -> Printf.ksprintf failwith "%S returned non-zero" cmd

let command_unit_or_fail cmd =
  let (_ : string list) = command_to_string_list_or_fail cmd in
  ()

let write_lines p l =
  let o = Caml.open_out p in
  List.iter l ~f:(Caml.Printf.fprintf o "%s\n") ;
  Caml.close_out o

let pp_to_file path f =
  let o = Caml.open_out path in
  let ppf = Caml.Format.formatter_of_out_channel o in
  f ppf () ;
  Caml.Format.pp_print_flush ppf () ;
  Caml.close_out o

let read_lines p =
  let open Caml in
  let o = open_in p in
  let r = ref [] in
  try
    while true do
      r := input_line o :: !r
    done ;
    assert false
  with _ -> close_in o ; List.rev !r
