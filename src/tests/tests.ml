open! Base
open! Merbocop_lib

module Mock_json = struct
  (** [test_json] is the json file pass to expecte_tests. *)
  let test_json (file : string) : Jq.t =
    Stdio.In_channel.read_all ("test_json/" ^ file) |> Jq.from_string

  let merge_request : Jq.t = test_json "get_mr"
  let project : Jq.t = test_json "get_project"
end

let mock_project : Project.t = Project.of_json Mock_json.project

(* A mock merge request for testing. *)
let mock_merge_request : Merge_request.t =
  let open Jq in
  let dict = get_dict Mock_json.merge_request in
  let gs s = get_field s dict |> get_string in
  let gb s = get_field s dict |> get_bool in
  let gi s = get_field s dict |> get_int in
  { id= gi "iid"
  ; title= gs "title"
  ; description= gs "description"
  ; project= mock_project
  ; comments= None
  ; threads= None
  ; commits= []
  ; changes= []
  ; last_update= gs "updated_at"
  ; labels= get_field "labels" dict |> get_strings
  ; approvals= {approvals_required= 0; approved_by= []}
  ; target_branch= {name= "mock-branch"; project= mock_project}
  ; touched_files= []
  ; reports= {conditionals= []}
  ; codeowners= None
  ; author= {id= 000000; username= "mr_author"; name= "Author of MR"}
  ; reviewers= []
  ; wip= gb "work_in_progress"
  ; allow_maintainer_to_push=
      (try Some (gb "allow_maintainer_to_push") with _ -> None)
  ; json= `O dict }

(* This test passes a list of mock merge requestes results to Comment_body.mr_section. *)
(* New mocks can be added by using the [mock_merge_request] above with the  [with] record *)
(* sytax to set sepcific fields. N *)
let mock_mr_results : Merge_request.mr_result list =
  let all_mrs : Merge_request.t list =
    [ { mock_merge_request with
        id= 1
      ; title= "Changes to alpha_context.ml without label or title prototype"
      ; changes=
          Change.(
            mock_change "Mock sha MR 1"
              [ mock_path "src/proto_alpha/lib_protocol/alpha_context.ml"
                  "src/proto_alpha/lib_protocol/alpha_context.ml" false ]
            |> of_json)
      ; codeowners= Codeowners.(of_json mockup_codeonwer) }
    ; { mock_merge_request with
        id= 2
      ; title= "Changes to alpha_context.ml with label"
      ; labels= ["proto"]
      ; changes=
          Change.(
            mock_change "Mock sha MR 2"
              [ mock_path "src/proto_alpha/lib_protocol/alpha_context.ml"
                  "src/proto_alpha/lib_protocol/alpha_context.ml" false ]
            |> of_json)
      ; codeowners= Codeowners.(of_json mockup_codeonwer) }
    ; { mock_merge_request with
        id= 3
      ; title= "Proto: changes to alpha_context.ml with title tags"
      ; changes=
          Change.(
            mock_change "Mock sha MR 3"
              [ mock_path "src/proto_alpha/lib_protocol/alpha_context.ml"
                  "src/proto_alpha/lib_protocol/alpha_context.ml" false ]
            |> of_json)
      ; codeowners= Codeowners.(of_json mockup_codeonwer) } ] in
  List.map all_mrs ~f:(fun mr -> Ok mr)

let%expect_test "Comment_body.mr_section TEST" =
  List.concat_mapi mock_mr_results ~f:(fun ith mr ->
      Comment_body.mr_section ~ith ~mr_list:mock_mr_results mr )
  |> String.concat ~sep:"\n" |> Debug.expect ;
  [%expect
    {|
### [1/3] Merge-Request [goldilocks/merbocop-testing!1](https://gitlab.com/goldilocks/merbocop-testing/merge_requests/1)

<span style="color: #700">Changes to alpha_context.ml without label or title prototype</span>

#### Bot-Message

Thank you for your contribution 🙏, I have a couple of remarks:

* ⚠ There are changes to proto-alpha:
    * → `src/proto_alpha/lib_protocol/alpha_context.ml`
    * → Please make sure the MR title contains `Proto` and/or add the `proto` label.
* 🤷 The merge-request description is empty.

Report (Project:36709623 MR:1):

* This MR has the exact number of apporvals required to be merged. Merbocop would add the `_Approved_Label_` when used with the `--set-approved-label` option.
* Merbocop wouldn't add the `_Doc_Label_` when used with the `--set-doc-label` option.

### [2/3] Merge-Request [goldilocks/merbocop-testing!2](https://gitlab.com/goldilocks/merbocop-testing/merge_requests/2)

<span style="color: #700">Changes to alpha_context.ml with label</span>

#### Bot-Message

Thank you for your contribution 🙏, I have one remark:

* 🤷 The merge-request description is empty.

Report (Project:36709623 MR:2):

* This MR has the exact number of apporvals required to be merged. Merbocop would add the `_Approved_Label_` when used with the `--set-approved-label` option.
* Merbocop wouldn't add the `_Doc_Label_` when used with the `--set-doc-label` option.

### [3/3] Merge-Request [goldilocks/merbocop-testing!3](https://gitlab.com/goldilocks/merbocop-testing/merge_requests/3)

<span style="color: #700">Proto: changes to alpha_context.ml with title tags</span>

#### Bot-Message

Thank you for your contribution 🙏, I have one remark:

* 🤷 The merge-request description is empty.

Report (Project:36709623 MR:3):

* This MR has the exact number of apporvals required to be merged. Merbocop would add the `_Approved_Label_` when used with the `--set-approved-label` option.
* Merbocop wouldn't add the `_Doc_Label_` when used with the `--set-doc-label` option. |}]

(* This test passes 4 merge requests 2 times. Once with a CODEOWNERS file and once without.*)

let mock_mr_list_1 : Merge_request.t list =
  (* Codeowners.json_mockups : Jq.t list contains one codeonwers file and a 404 not found. *)
  List.concat_mapi Codeowners.json_mockups ~f:(fun ico co ->
      (* Change.json_mockups : Jq.t list contains five mr.changes of 1-2 paths each. *)
      List.mapi Change.json_mockups ~f:(fun ich ch ->
          { mock_merge_request with
            id= (ico + 1) * (ich + 1)
          ; changes= Change.of_json ch
          ; codeowners= Codeowners.of_json co } ) )

let%expect_test "Reviewers.of_changes TEST" =
  let open Reviewers in
  List.iter mock_mr_list_1 ~f:(fun mr ->
      Fmt.pr "[Merge-Request-%d:]\n%!" mr.id ;
      of_changes ~mr |> show |> Debug.expect ) ;
  [%expect
    {|
    [Merge-Request-1:]
       ["docs_owner"; "lib_p2p_owner"; "mr_author"; "not_a_member"]
    [Merge-Request-2:]
       ["lib_p2p_owner"; "mr_author"]
    [Merge-Request-3:]
       ["lib_benchmark_owner"; "mr_author"]
    [Merge-Request-4:]
       ["gitlab_owner"; "mr_author"]
    [Merge-Request-5:]
       []
    [Merge-Request-2:]
       []
    [Merge-Request-4:]
       []
    [Merge-Request-6:]
       []
    [Merge-Request-8:]
       []
    [Merge-Request-10:]
       [] |}]

let%expect_test "Reviewers.new_reviewers_and_report TEST" =
  let open Reviewers in
  List.iter mock_mr_list_1 ~f:(fun mr ->
      Fmt.pr "[Merge-Request-%d:]\n%!" mr.id ;
      new_reviewers_and_report ~mr |> show |> Debug.expect ) ;
  [%expect
    {|
    [Merge-Request-1:]
       ["not_a_member"; "lib_p2p_owner"; "docs_owner"]
    [Merge-Request-2:]
       ["lib_p2p_owner"]
    [Merge-Request-3:]
       ["lib_benchmark_owner"]
    [Merge-Request-4:]
       ["gitlab_owner"]
    [Merge-Request-5:]
       []
    [Merge-Request-2:]
       []
    [Merge-Request-4:]
       []
    [Merge-Request-6:]
       []
    [Merge-Request-8:]
       []
    [Merge-Request-10:]
       [] |}]

let%expect_test "Project.of_json TEST" =
  let open Project in
  mock_project |> show |> Debug.expect ;
  [%expect
    {|
{ Project.id = 36709623; path_with_namespace = "goldilocks/merbocop-testing";
  http_url_to_repo = "https://gitlab.com/goldilocks/merbocop-testing.git";
  web_url = "https://gitlab.com/goldilocks/merbocop-testing";
  json =
  `O ([("id", `Float (36709623.)); ("description", `String (""));
        ("name", `String ("tezos-merbocop-testing"));
        ("name_with_namespace",
         `String ("Goldilocks / tezos-merbocop-testing"));
        ("path", `String ("merbocop-testing"));
        ("path_with_namespace", `String ("goldilocks/merbocop-testing"));
        ("created_at", `String ("2022-06-02T19:58:31.015Z"));
        ("default_branch", `String ("master")); ("tag_list", `A ([]));
        ("topics", `A ([]));
        ("ssh_url_to_repo",
         `String ("git@gitlab.com:goldilocks/merbocop-testing.git"));
        ("http_url_to_repo",
         `String ("https://gitlab.com/goldilocks/merbocop-testing.git"));
        ("web_url",
         `String ("https://gitlab.com/goldilocks/merbocop-testing"));
        ("readme_url",
         `String ("https://gitlab.com/goldilocks/merbocop-testing/-/blob/master/README.md"));
        ("avatar_url", `Null); ("forks_count", `Float (1.));
        ("star_count", `Float (1.));
        ("last_activity_at", `String ("2022-06-23T15:33:18.882Z"));
        ("namespace",
         `O ([("id", `Float (2418169.)); ("name", `String ("Goldilocks"));
               ("path", `String ("goldilocks")); ("kind", `String ("user"));
               ("full_path", `String ("goldilocks")); ("parent_id", `Null);
               ("avatar_url", `String ("none"));
               ("web_url", `String ("https://gitlab.com/goldilocks"))]));
        ("container_registry_image_prefix",
         `String ("registry.gitlab.com/goldilocks/merbocop-testing"));
        ("_links",
         `O ([("self",
               `String ("https://gitlab.com/api/v4/projects/36709623"));
               ("issues",
                `String ("https://gitlab.com/api/v4/projects/36709623/issues"));
               ("merge_requests",
                `String ("https://gitlab.com/api/v4/projects/36709623/merge_requests"));
               ("repo_branches",
                `String ("https://gitlab.com/api/v4/projects/36709623/repository/branches"));
               ("labels",
                `String ("https://gitlab.com/api/v4/projects/36709623/labels"));
               ("events",
                `String ("https://gitlab.com/api/v4/projects/36709623/events"));
               ("members",
                `String ("https://gitlab.com/api/v4/projects/36709623/members"));
               ("cluster_agents",
                `String ("https://gitlab.com/api/v4/projects/36709623/cluster_agents"))
               ]));
        ("packages_enabled", `Bool (true)); ("empty_repo", `Bool (false));
        ("archived", `Bool (false)); ("visibility", `String ("public"));
        ("owner",
         `O ([("id", `Float (1952891.));
               ("username", `String ("goldilocks"));
               ("name", `String ("Goldilocks"));
               ("state", `String ("active"));
               ("avatar_url", `String ("https://gitlab.comnone"));
               ("web_url", `String ("https://gitlab.com/goldilocks"))]));
        ("resolve_outdated_diff_discussions", `Bool (false));
        ("container_expiration_policy",
         `O ([("cadence", `String ("1d")); ("enabled", `Bool (false));
               ("keep_n", `Float (10.)); ("older_than", `String ("90d"));
               ("name_regex", `String (".*")); ("name_regex_keep", `Null);
               ("next_run_at", `String ("2022-06-03T19:58:31.047Z"))]));
        ("issues_enabled", `Bool (true));
        ("merge_requests_enabled", `Bool (true));
        ("wiki_enabled", `Bool (false)); ("jobs_enabled", `Bool (true));
        ("snippets_enabled", `Bool (false));
        ("container_registry_enabled", `Bool (true));
        ("service_desk_enabled", `Bool (true));
        ("service_desk_address",
         `String ("contact-project+goldilocks-merbocop-testing-36709623-issue-@incoming.gitlab.com"));
        ("can_create_merge_request_in", `Bool (true));
        ("issues_access_level", `String ("enabled"));
        ("repository_access_level", `String ("enabled"));
        ("merge_requests_access_level", `String ("enabled"));
        ("forking_access_level", `String ("enabled"));
        ("wiki_access_level", `String ("disabled"));
        ("builds_access_level", `String ("enabled"));
        ("snippets_access_level", `String ("disabled"));
        ("pages_access_level", `String ("enabled"));
        ("operations_access_level", `String ("enabled"));
        ("analytics_access_level", `String ("enabled"));
        ("container_registry_access_level", `String ("enabled"));
        ("security_and_compliance_access_level", `String ("private"));
        ("emails_disabled", `Null);
        ("shared_runners_enabled", `Bool (false));
        ("lfs_enabled", `Bool (true)); ("creator_id", `Float (1952891.));
        ("forked_from_project",
         `O ([("id", `Float (3836952.)); ("description", `String (""));
               ("name", `String ("tezos"));
               ("name_with_namespace", `String ("Tezos / tezos"));
               ("path", `String ("tezos"));
               ("path_with_namespace", `String ("tezos/tezos"));
               ("created_at", `String ("2017-08-02T12:26:47.101Z"));
               ("default_branch", `String ("master")); ("tag_list", `A ([]));
               ("topics", `A ([]));
               ("ssh_url_to_repo", `String ("git@gitlab.com:tezos/tezos.git"));
               ("http_url_to_repo",
                `String ("https://gitlab.com/tezos/tezos.git"));
               ("web_url", `String ("https://gitlab.com/tezos/tezos"));
               ("readme_url",
                `String ("https://gitlab.com/tezos/tezos/-/blob/master/README.md"));
               ("avatar_url",
                `String ("https://gitlab.com/uploads/-/system/project/avatar/3836952/latin_small_letter_tz.png"));
               ("forks_count", `Float (319.)); ("star_count", `Float (543.));
               ("last_activity_at", `String ("2022-07-13T19:46:05.986Z"));
               ("namespace",
                `O ([("id", `Float (1385864.)); ("name", `String ("Tezos"));
                      ("path", `String ("tezos"));
                      ("kind", `String ("group"));
                      ("full_path", `String ("tezos")); ("parent_id", `Null);
                      ("avatar_url",
                       `String ("/uploads/-/system/group/avatar/1385864/latin_small_letter_tz.png"));
                      ("web_url", `String ("https://gitlab.com/groups/tezos"))
                      ]))
               ]));
        ("mr_default_target_self", `Bool (true)); ("import_url", `Null);
        ("import_type", `Null); ("import_status", `String ("finished"));
        ("import_error", `Null); ("open_issues_count", `Float (0.));
        ("runners_token", `String ("lkjllkjlU-g7lkjGBljyRxkjMt"));
        ("ci_default_git_depth", `Float (0.));
        ("ci_forward_deployment_enabled", `Bool (true));
        ("ci_job_token_scope_enabled", `Bool (false));
        ("ci_separated_caches", `Bool (true));
        ("ci_opt_in_jwt", `Bool (false)); ("public_jobs", `Bool (true));
        ("build_git_strategy", `String ("fetch"));
        ("build_timeout", `Float (3600.));
        ("auto_cancel_pending_pipelines", `String ("enabled"));
        ("build_coverage_regex", `Null); ("ci_config_path", `String (""));
        ("shared_with_groups", `A ([]));
        ("only_allow_merge_if_pipeline_succeeds", `Bool (false));
        ("allow_merge_on_skipped_pipeline", `Bool (false));
        ("restrict_user_defined_variables", `Bool (false));
        ("request_access_enabled", `Bool (true));
        ("only_allow_merge_if_all_discussions_are_resolved", `Bool (false));
        ("remove_source_branch_after_merge", `Bool (true));
        ("printing_merge_request_link_enabled", `Bool (true));
        ("merge_method", `String ("merge"));
        ("squash_option", `String ("default_off"));
        ("enforce_auth_checks_on_uploads", `Bool (true));
        ("suggestion_commit_message", `String (""));
        ("merge_commit_template", `Null); ("squash_commit_template", `Null);
        ("auto_devops_enabled", `Bool (false));
        ("auto_devops_deploy_strategy", `String ("continuous"));
        ("autoclose_referenced_issues", `Bool (true));
        ("keep_latest_artifact", `Bool (true));
        ("runner_token_expiration_interval", `Null);
        ("external_authorization_classification_label", `String (""));
        ("requirements_enabled", `Bool (false));
        ("requirements_access_level", `String ("enabled"));
        ("security_and_compliance_enabled", `Bool (true));
        ("compliance_frameworks", `A ([]));
        ("permissions",
         `O ([("project_access",
               `O ([("access_level", `Float (40.));
                     ("notification_level", `Float (3.))]));
               ("group_access", `Null)]))
        ])
  } |}]
